terraform {
  backend "s3" {
    bucket = "dibo-devops-dev-01"
    key = "dibo-devops-dev-01/terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "terraform-state-lock-dynamo"
  }
}

/*
when creating the Dynamp DB table, make sure to create partition key as "LockID".
*/ 

